package spotifyplayer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;


public class SpotifyController{
    final static private String SPOTIFY_CLIENT_ID     = "c53a4fc1656042d09fd56d8859ddd634";
    final static private String SPOTIFY_CLIENT_SECRET = "b3c1b0bd9fa74c8891eed34a6c2bf038";
    
    public static String getArtistId(String artistNameQuery)
    {
        String artistId = "";
        
        try
        {
            // TODO - From an artist string, get the spotify ID
            // Recommended Endpoint: https://api.spotify.com/v1/search
            // Parse the JSON output to retrieve the ID
        
            String endpoint = "https://api.spotify.com/v1/search";
            String params = "type=artist&q=" + artistNameQuery;
            String jsonOutput = spotifyEndpointToJson(endpoint, params);
            
            // TODO - Parse the JSON output in order to retrieve the artist id
            JsonObject root = new JsonParser().parse(jsonOutput).getAsJsonObject();
            JsonObject artist = root.get("artists").getAsJsonObject();
            JsonArray items = artist.get("items").getAsJsonArray();
            
            if(items.size() > 0) {
                JsonObject firstItem = items.get(0).getAsJsonObject();
                artistId = firstItem.get("id").getAsString();
            }
            else
                artistId = "3WrFJ7ztbogyGnTHbHJFl2"; 
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return artistId;
    }
    
    public static ArrayList<String> getAlbumIdsFromArtist(String artistId)
    {
        ArrayList<String> albumIds = new ArrayList<>();
        
        try
        {
            // TODO - Retrieve album ids from an artist id
            // Recommended endpoint {id} is the id of the artist in parameter: 
            //             https://api.spotify.com/v1/artists/{id}/albums
            //
            // Arguments - Filter for the CA market, and limit to 50 albums
            String endpoint = "https://api.spotify.com/v1/artists/"+artistId+"/albums";
            String params = "market=CA&limit=50";
            String jsonOutput = spotifyEndpointToJson(endpoint,params);
            JsonObject root = new JsonParser().parse(jsonOutput).getAsJsonObject();
            JsonArray items = root.get("items").getAsJsonArray();
            
            for(int i = 0; i < items.size(); i++) {
                JsonObject firstItem = items.get(i).getAsJsonObject();
                String albumId = firstItem.get("id").getAsString();
                albumIds.add(albumId);
                //System.out.println(albumId);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return albumIds;
    }
    
    public static ArrayList<Album> getAlbumDataFromArtist(String artistId)
    {
        ArrayList<String> albumIds = getAlbumIdsFromArtist(artistId);
        ArrayList<Album> albums = new ArrayList<>();
        
        for(String albumId : albumIds)
        {
            try
            {
                // TODO - Retrieve all album data from the list of album ids for an artist
                // 
                // You can have a look at the Album class included
                // 
                // Endpoint : https://api.spotify.com/v1/albums/{id}
                // Note:      {id} is the id of the album
                //
                // Arguments - Filter for the CA market
                ArrayList<Track> albumTracks = new ArrayList<>();
                
                String endpoint = "https://api.spotify.com/v1/albums/"+albumId;
                String params = "market=CA";
                String jsonOutput = spotifyEndpointToJson(endpoint,params);
                JsonObject root = new JsonParser().parse(jsonOutput).getAsJsonObject();
                
                JsonArray artists = root.get("artists").getAsJsonArray();
                JsonObject artist = artists.get(0).getAsJsonObject();
                String artistName = artist.get("name").getAsString();
                String albumName = root.get("name").getAsString();
                
                JsonArray images = root.get("images").getAsJsonArray();
                JsonObject image = images.get(0).getAsJsonObject();
                String coverURL = image.get("url").getAsString();
                
                JsonObject tracks = root.get("tracks").getAsJsonObject();
                JsonArray trackItems = tracks.get("items").getAsJsonArray();
                
                for(int i = 0; i < trackItems.size();i++) {
                    JsonObject track = trackItems.get(i).getAsJsonObject();
                    int trackNum = i+1;
                    
                    String trackName = track.get("name").getAsString();
                    
                    String durationStr = track.get("duration_ms").getAsString();
                    int durationInt  = Integer.parseInt(durationStr)/1000;
                    
                    String previewUrl = "";
                    if (track.get("preview_url").isJsonNull() == false)
                        previewUrl = track.get("preview_url").getAsString();
                    
                    albumTracks.add(new Track(trackNum, trackName, durationInt, previewUrl));
                }
                albums.add(new Album(artistName, albumName,  coverURL, albumTracks));                
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }            
        }
        
        return albums;
    }


    // This code will help you retrieve the JSON data from a spotify end point
    // It takes care of the complicated parts such as the authentication and 
    // connection to the Web API
    // 
    // You shouldn't have to modify any of the code...
    private static String spotifyEndpointToJson(String endpoint, String params)
    {
        params = params.replace(' ', '+');

        try
        {
            String fullURL = endpoint;
            if (params.isEmpty() == false)
            {
                fullURL += "?"+params;
            }
            
            URL requestURL = new URL(fullURL);
            
            HttpURLConnection connection = (HttpURLConnection)requestURL.openConnection();
            String bearerAuth = "Bearer " + getSpotifyAccessToken();
            connection.setRequestProperty ("Authorization", bearerAuth);
            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String inputLine;
            String jsonOutput = "";
            while((inputLine = in.readLine()) != null)
            {
                jsonOutput += inputLine;
            }
            in.close();
            
            return jsonOutput;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return "";
    }


    // This implements the Client Credentials Authorization Flows
    // Based on the Spotify API documentation
    // 
    // It retrieves the Access Token based on the client ID and client Secret  
    //
    // You shouldn't have to modify any of this code...          
    private static String getSpotifyAccessToken()
    {
        try
        {
            URL requestURL = new URL("https://accounts.spotify.com/api/token");
            
            HttpURLConnection connection = (HttpURLConnection)requestURL.openConnection();
            String keys = SPOTIFY_CLIENT_ID+":"+SPOTIFY_CLIENT_SECRET;
            String postData = "grant_type=client_credentials";
            
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(keys.getBytes()));
            
            // Send header parameter
            connection.setRequestProperty ("Authorization", basicAuth);
            
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);

            // Send body parameters
            OutputStream os = connection.getOutputStream();
            os.write( postData.getBytes() );
            os.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            
            String inputLine;
            String jsonOutput = "";
            while((inputLine = in.readLine()) != null)
            {
                jsonOutput += inputLine;
            }
            in.close();
            
            JsonElement jelement = new JsonParser().parse(jsonOutput);
            JsonObject rootObject = jelement.getAsJsonObject();
            String token = rootObject.get("access_token").getAsString();

            return token;
        }
        catch(Exception e)
        {
            System.out.println("Something wrong here... make sure you set your Client ID and Client Secret properly!");
            e.printStackTrace();
        }
        
        return "";
    }
}
