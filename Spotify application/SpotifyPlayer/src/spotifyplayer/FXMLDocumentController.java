package spotifyplayer;


import com.sun.javafx.collections.ObservableListWrapper;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Callback;
import javafx.util.Duration;

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label artistLabel;
    
    @FXML
    private Label albumLabel;
    
    @FXML
    private ImageView albumCoverImageView;

    @FXML
    private TextField searchArtistTextField;
    
    @FXML
    private Button previousAlbumButton;
    
    @FXML
    private Button nextAlbumButton;
    
    @FXML
    private TableView tracksTableView;

    @FXML
    private Button playButton;
    
    @FXML
    private Button mainPlayButton;
    
    @FXML
    private Slider trackSlider;
    
    @FXML
    private Label trackTimeLabel;
        
    @FXML
    private ProgressIndicator progress;
    
    MediaPlayer mediaPlayer = null;
    int currentAlbum = 0;
    ArrayList<Album> albums = null;
    Track currentTrack = null;
    ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    @FXML
    private void handlePlayButtonAction(ActionEvent event) throws InterruptedException {
        if (playButton.getText().equals("Play")) 
        {   //play music
            playButton.setText("Pause");
            
            if (mediaPlayer != null)
            {
                mediaPlayer.seek(Duration.millis(trackSlider.getValue()*1000));
                mediaPlayer.play(); 
                moveSlider();
            }
        }
        else
        {   //pause music
            playButton.setText("Play");
            if (mediaPlayer != null)
            {
                mediaPlayer.pause();  
                executor.shutdown();
                executor.awaitTermination(1, TimeUnit.SECONDS);
                executor = Executors.newSingleThreadScheduledExecutor();
            }
        }
    }

    @FXML
    private void handlePreviousButtonAction(ActionEvent event) {
        if((currentAlbum-1) < 0) {
            currentAlbum = albums.size()-1;
            displayAlbum(currentAlbum);
        }
        else {
            currentAlbum --;
            displayAlbum(currentAlbum);
        }
    }

    @FXML
    private void handleNextButtonAction(ActionEvent event) { 
        if((currentAlbum+1) > (albums.size()-1)) {
            currentAlbum = 0;
            displayAlbum(currentAlbum);
        }
        else {
            currentAlbum++;
            displayAlbum(currentAlbum);
        }
    }
    
    @FXML
    private void handleSearchButtonAction(ActionEvent event) {
        //check if no artist given or no artist found
        progress.setVisible(true);
        searchArtistTextField.setDisable(true);
        previousAlbumButton.setDisable(true);
        nextAlbumButton.setDisable(true);

        // This insures we don't block the GUI when executing slow Web Requests
        
        ExecutorService executer = Executors.newSingleThreadScheduledExecutor();
        executer.submit(new Task<Void>(){ //crashes here
            @Override
            protected Void call() throws Exception {
                // You can't change any JavaFX Controls here...
                //make sure it doesnt do anything with the JavaFX controls
                try {
                searchArtist(searchArtistTextField.getText());
                }
                catch(Exception e){
                    failed();
                }
                return null;
            }

            @Override
            protected void succeeded() {
                progress.setVisible(false);
                searchArtistTextField.setDisable(false);

                if (albums.size() > 0)
                {
                    currentAlbum = 0;
                    displayAlbum(currentAlbum);
                }
            }
            @Override
            protected void failed() {
                progress.setVisible(false);
                searchArtistTextField.setDisable(false);
                String searchField = "";
                if (searchArtistTextField.getText() != null)
                    albumLabel.setText("Error retrieving " + searchArtistTextField.getText());
                else
                    albumLabel.setText("Error retrieving " +searchField );
                
                artistLabel.setText("Error!");
            }
            @Override
            protected void cancelled() { 
                progress.setVisible(false);
                searchArtistTextField.setDisable(false);

                artistLabel.setText("Error!");
                albumLabel.setText("Error retrieving " + searchArtistTextField.getText());
            }            
        });
        
    }
    
    private void displayAlbum(int albumIndex)
    {
        if (albumIndex >=0 && albumIndex < albums.size())
        {
            Album album = albums.get(albumIndex);
            System.out.println(album);
            
            artistLabel.setText(album.getArtistName());
            albumLabel.setText(album.getAlbumName());

            // Set tracks
            ArrayList<TrackForTableView> tracks = new ArrayList<>();
            for (int i=0; i<album.getTracks().size(); ++i)
            {
                TrackForTableView trackForTable = new TrackForTableView();
                Track track = album.getTracks().get(i);
                trackForTable.setTrackNumber(track.getNumber());
                trackForTable.setTrackTitle(track.getTitle());
                trackForTable.setTrackPreviewUrl(track.getUrl());
                tracks.add(trackForTable);
            }
            tracksTableView.setItems(new ObservableListWrapper(tracks));            
                        
            // Previous and next buttons 
            if (albums.size() > 1)
            {
                previousAlbumButton.setDisable(false);
                nextAlbumButton.setDisable(false);
            }
            else
            {
                previousAlbumButton.setDisable(true);
                nextAlbumButton.setDisable(true);
            }

            // Cover image
            Image coverImage = new Image(album.getImageURL());
            albumCoverImageView.setImage(coverImage);
            
            // Track 1 slider / time information
            int minutes = album.getTracks().get(0).getDurationInSeconds() / 60;
            int seconds = album.getTracks().get(0).getDurationInSeconds() % 60;
            String secondsStr = seconds < 10 ? "0" + seconds : "" + seconds;
            
            //trackSlider.setMax(30.0);
            //trackSlider.setMin(0.0);
            trackSlider.setValue(0.0);
            trackTimeLabel.setText("0.00 / " + minutes + ":" + secondsStr);
        }
    }
    
    private void searchArtist(String artistName)
    {
        String artistId = SpotifyController.getArtistId(artistName);
        currentAlbum = 0;
        albums = SpotifyController.getAlbumDataFromArtist(artistId);   
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Setup Table View
        TableColumn<TrackForTableView, Number> trackNumberColumn = new TableColumn("#");
        trackNumberColumn.setCellValueFactory(new PropertyValueFactory("trackNumber"));
        
        TableColumn trackTitleColumn = new TableColumn("Title");
        trackTitleColumn.setCellValueFactory(new PropertyValueFactory("trackTitle"));
        trackTitleColumn.setPrefWidth(250);
        
        TableColumn playColumn = new TableColumn("Preview");
        playColumn.setCellValueFactory(new PropertyValueFactory("trackPreviewUrl"));
        Callback<TableColumn<TrackForTableView, String>, TableCell<TrackForTableView, String>> cellFactory = new Callback<TableColumn<TrackForTableView, String>, TableCell<TrackForTableView, String>>(){
            @Override
            public TableCell<TrackForTableView, String> call(TableColumn<TrackForTableView, String> param) {
                final TableCell<TrackForTableView, String> cell = new TableCell<TrackForTableView, String>(){
                    final Button finalplayButton = new Button("Play");

                    @Override
                    public void updateItem(String item, boolean empty)
                    {
                        if (item != null && item.equals("") == false)
                        {
                            finalplayButton.setOnAction(event -> {
                                executor.shutdown();
                                try {
                                    executor.awaitTermination(1, TimeUnit.SECONDS);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                finally {
                                    executor = Executors.newSingleThreadScheduledExecutor();
                                }
                                if (mediaPlayer != null)
                                {
                                    mediaPlayer.stop();                                    
                                }
                                
                                Media music = new Media(item);
                                mediaPlayer = new MediaPlayer(music);
                                mediaPlayer.play();
                                playButton.setText("Pause");
                                
                                trackSlider.setValue(0);
                                trackSlider.setMax(30);
                                trackSlider.setMin(0);
                                trackTimeLabel.setText("0.0 / 30.0");
                                moveSlider();
                            });
                            setGraphic(finalplayButton);
                        }
                        else
                        {                        
                            setGraphic(null);
                        }
                        setText(null);
                    }
                };
                return cell;
            }
        };
        playColumn.setCellFactory(cellFactory);
        
        //move to random position slider
        trackSlider.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (mediaPlayer != null)
                {
                    mediaPlayer.pause();
                    executor.shutdown();
                    try {
                        executor.awaitTermination(1, TimeUnit.SECONDS);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    finally {
                        executor = Executors.newSingleThreadScheduledExecutor();
                    }
                }
            }
        
        });
        trackSlider.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (mediaPlayer != null)
                {
                    mediaPlayer.seek(Duration.millis(trackSlider.getValue()*1000));
                    mediaPlayer.play(); 
                    moveSlider();
                }
            }
        
        });
        
        tracksTableView.getColumns().setAll(trackNumberColumn, trackTitleColumn, playColumn);
        
        searchArtist("pink floyd");
        displayAlbum(0);
    }  
    
    public void moveSlider() {
        executor.scheduleAtFixedRate(new Runnable(){ 
            @Override
            public void run() {
                // You can't change any JavaFX Controls here...

                Platform.runLater(new Runnable() {
                   @Override
                   public void run() {
                       //Move slider by 1
                       trackSlider.setValue(Math.round(trackSlider.getValue())+1);
                       System.out.println(Math.round(trackSlider.getValue()));
                       trackTimeLabel.setText(Math.round(trackSlider.getValue())+" / 30.0");
                       //if slider reaches the end
                       if(trackSlider.getValue()==30) {
                            mediaPlayer.pause();
                            executor.shutdown();
                            trackSlider.setValue(0);
                            trackTimeLabel.setText(Math.round(trackSlider.getValue())+" / 30.0");
                            mediaPlayer.seek(Duration.millis(trackSlider.getValue()*1000));
                            try {
                                executor.awaitTermination(1, TimeUnit.SECONDS);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            finally {
                                mediaPlayer.play();
                                executor = Executors.newSingleThreadScheduledExecutor();
                                playButton.setText("Pause");
                            }                            
                            moveSlider();
                       }
                   }
                });
            }
        }, 1, 1, TimeUnit.SECONDS);
    }
}
